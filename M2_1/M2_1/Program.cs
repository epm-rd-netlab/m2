﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M2_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int[] number_1 = new int[32];
            int[] number_2 = new int[32];
            for(int p=0;p<23;p++)
            {
                number_1[p] = rand.Next(0, 2);
                number_2[p] = rand.Next(0, 2);

            }
            number_1[31] = rand.Next(0, 1);
            number_2[31] = rand.Next(0, 1);
            string str1 = "";
            string str2 = "";
            for (int p = 31; p >= 0; p--)
            {
                str1 +=Convert.ToString(number_1[p]);
                str2 += Convert.ToString(number_2[p]);
            }
            Console.WriteLine("Initial data: ");
            Console.WriteLine(str1);
            Console.WriteLine(str2);
            int i= rand.Next(0, 23);
            int j= rand.Next(i+1, 23);
            Console.WriteLine("from "+ i +" to "+j );
            int u = 0;
            str2 = "";
            for (int p = i; p <= j; p++)
            {
                number_2[p] = number_1[u];
                u++;
            }
            for (int p = 31; p >= 0; p--)
            {
                str2 += Convert.ToString(number_2[p]);
            }
            Console.WriteLine("Result: "+ str2);
            Console.ReadKey();
        }
    }
}
