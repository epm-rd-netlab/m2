﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace m2_4
{
    class Program
    {
        //Реализовать алгоритм конкатенации двух строк, содержащих только символы латинского алфавита, исключая повторяющиеся символы.
        static void Main(string[] args)
        {
            string str1 = "Hello, boy!";
            string str2 = "Hello, man!";
            Console.WriteLine("merge two strings: " + merge(str1, str2));
            Console.ReadKey();
        }
        static string merge(string str1, string str2)
        {
            string str = " ";

            str = str1 + str2;
            str = new string(str.Distinct().ToArray());

            return str;
        }
    }
}
