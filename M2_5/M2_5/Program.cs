﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M2_5
{
    //Реализовать метод FindNextBiggerNumber, который принимает положительное целое число и
    //    возвращает ближайшее наибольшее целое, состоящее из цифр исходного числа, и null (или -1), если такого числа не существует.
    class Program
    {
        static void Main(string[] args)
        {
            
            int[] number = new int[] { 3, 4, 5, 6, 4, 3, 2};
            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();
            
            myStopwatch.Stop(); //остановить
                    
            string str = "";
            for(int i=0;i<number.Length;i++)
            {
                str += number[i];
            }
            int max = Convert.ToInt32(str);
            int min = Convert.ToInt32(str);
            myStopwatch.Start(); //запуск
            FindNextBiggerNumber(number, ref max, min);
            myStopwatch.Stop(); //остановить
            Console.WriteLine( max);
            TimeSpan ts = myStopwatch.Elapsed;
            Console.WriteLine("lead time: "+ ts.Milliseconds+ "ms");
            // Console.WriteLine(number + ", ExpectedResult = " + FindNextBiggerNumber(number));
            Console.ReadKey();
        }
        public static void FindNextBiggerNumber<T>(IList<T> arr,ref int max, int min, string current = "")
        {
            
            if (arr.Count == 0) //если все элементы использованы, выводим на консоль получившуюся строку и возвращаемся
            {
                if(max==min&& min<Convert.ToInt32(current))
                {
                    max = Convert.ToInt32(current);
                }
                if (max > Convert.ToInt32(current) && min < Convert.ToInt32(current))
                {
                    max = Convert.ToInt32(current);
                }
 
                return ;
            }
            for (int i = 0; i < arr.Count; i++) //в цикле для каждого элемента прибавляем его к итоговой строке, создаем новый список из оставшихся элементов, и вызываем эту же функцию рекурсивно с новыми параметрами.
            {
                List<T> lst = new List<T>(arr);
                lst.RemoveAt(i);
                FindNextBiggerNumber(lst, ref max,min, current + arr[i].ToString());
            }
        }
    }
}
