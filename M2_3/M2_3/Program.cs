﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M2_3
{
    
//Реализовать алгоритм поиска в вещественном массиве индекса элемента,
//для которого сумма элементов слева и сумма элементов спарава равны.Если такого элемента не существует вернуть null (или -1).
    class Program
    {
        static void Main(string[] args)
        {
            //var a = new[] { 0, 5, 6, 1, 4, 7, 8, 3, 2, 4 };
            var a = new[] { 0, 4, 3, 1, 4, 7, 8, 3, 2, 4 };
            Console.WriteLine("item index: " + Search(a));
            Console.ReadKey();
        }
        static int Search(int[] mass)
        {
            for (int i = 2; i<mass.Length-2;i++)
            {
                if(mass[i-2]+mass[i-1]==mass[i+1]+mass[i+2])
                {
                    return i;
                }
            }

                return -1;
        }
    }
}
