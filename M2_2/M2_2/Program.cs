﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M2_2
{
    //Реализовать рекурсивный алгоритм поиска максимального элемента в неотсортированном целочисленом массиве.
    class Program
    {
        static void Main(string[] args)
        {
            var a = new[] { 0, 5, 6, 1, 9, 7, 8, 3, 2, 4 };
            Console.WriteLine(" Максимальный элемент массива: " + IndexOfMax(a, a.Length - 1));
            Console.ReadLine();
        }
        static int IndexOfMax(int[] array, int len)
        {
            if (len == 0)
            {
                return len;
            }

            var i = IndexOfMax(array, len - 1);
            return array[len] > array[i] ? len : array[len];
        }
    }
}
