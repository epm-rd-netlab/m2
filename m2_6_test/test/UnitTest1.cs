﻿using System;
using m2_6_test;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod7()
        {
            int[] mass = new int[] { 7, 1, 2, 3, 4, 5, 6, 7, 68, 69, 70, 15, 17 };
            Class1 test = new Class1();
            string actual = test.Search(mass, 7);
            Assert.AreEqual("7,7,70,17", actual);
        }
    }
}
