﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace m2_5_test
{
    public class fortest
    {
        public int FindNext(int[] number)
        {

            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();

            myStopwatch.Stop(); //остановить

            string str = "";
            for (int i = 0; i < number.Length; i++)
            {
                str += number[i];
            }
            int max = Convert.ToInt32(str);
            int min = Convert.ToInt32(str);
            myStopwatch.Start(); //запуск
            FindNextBiggerNumber(number, ref max, min);
            myStopwatch.Stop(); //остановить
            if (max != min)
                return max;
            else
                return -1;
 
            
        }
        public static void FindNextBiggerNumber<T>(IList<T> arr, ref int max, int min, string current = "")
        {

            if (arr.Count == 0) //если все элементы использованы, то сравниваем с максимумом и минимумом
            {
                if (max == min && min < Convert.ToInt32(current))
                {
                    max = Convert.ToInt32(current);
                }
                if (max > Convert.ToInt32(current) && min < Convert.ToInt32(current))
                {
                    max = Convert.ToInt32(current);
                }

                return;
            }
            for (int i = 0; i < arr.Count; i++) //в цикле для каждого элемента прибавляем его к итоговой строке, создаем новый список из оставшихся элементов, и вызываем эту же функцию рекурсивно с новыми параметрами.
            {
                List<T> lst = new List<T>(arr);
                lst.RemoveAt(i);
                FindNextBiggerNumber(lst, ref max, min, current + arr[i].ToString());
            }
        }
    }
}
