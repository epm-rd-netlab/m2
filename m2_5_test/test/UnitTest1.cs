﻿using System;
using m2_5_test;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMetho2017()
        {
            int[] number = new int[] { 2, 0, 1, 7 };
            fortest test = new fortest();
            int actual= test.FindNext(number);
            Assert.AreEqual(2071, actual);
        }
        [TestMethod]
        public void TestMetho21()
        {
            int[] number = new int[] { 1, 2 };
            fortest test = new fortest();
            int actual = test.FindNext(number);
            Assert.AreEqual(21, actual);
        }
        [TestMethod]
        public void TestMetho513()
        {
            int[] number = new int[] { 5, 1, 3 };
            fortest test = new fortest();
            int actual = test.FindNext(number);
            Assert.AreEqual(531, actual);
        }
        [TestMethod]
        public void TestMetho414()
        {
            int[] number = new int[] { 4,1,4 };
            fortest test = new fortest();
            int actual = test.FindNext(number);
            Assert.AreEqual(441, actual);
        }
     

        [TestMethod]
        public void TestMetho1234321()
        {
            int[] number = new int[] { 1,2,3,4,3,2,1 };
            fortest test = new fortest();
            int actual = test.FindNext(number);
            Assert.AreEqual(1241233, actual);
        }
        [TestMethod]
        public void TestMetho1234126()
        {
            int[] number = new int[] { 1,2,3,4,1,2,6 };
            fortest test = new fortest();
            int actual = test.FindNext(number);
            Assert.AreEqual(1234162, actual);
        }
        [TestMethod]
        public void TestMetho3456432()
        {
            int[] number = new int[] { 3,4,5,6,4,3,2 };
            fortest test = new fortest();
            int actual = test.FindNext(number);
            Assert.AreEqual(3462345, actual);
        }
        [TestMethod]
        public void TestMetho10()
        {
            int[] number = new int[] { 1,0};
            fortest test = new fortest();
            int actual = test.FindNext(number);
            Assert.AreEqual(-1, actual);

        }
        [TestMethod]
        public void TestMetho20()
        {
            int[] number = new int[] { 2, 0 };
            fortest test = new fortest();
            int actual = test.FindNext(number);
            Assert.AreEqual(-1, actual);

        }
    }
}
